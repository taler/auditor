#!/usr/bin/env python3
# This file is in the public domain.
#
# This script runs the jinga2 templating engine on an input template-file
# using the specified locale for gettext translations, and outputs
# the resulting (HTML) ouptut-file.
#
# Note that the gettext files need to be prepared first. This script
# is thus to be invoked via the Makefile.
import os
import os.path
import sys
import re
import gettext
import jinja2
import glob
import codecs
import os
import os.path
import subprocess

env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
        extensions=["jinja2.ext.i18n"],
        lstrip_blocks=True,
        trim_blocks=True,

        undefined=jinja2.StrictUndefined,
                         autoescape=False)

default_ctx = {}
default_ctx["merchant_blog_url"] = os.environ.get("TALER_ENV_URL_MERCHANT_BLOG", "#")
default_ctx["merchant_donations_url"] = os.environ.get("TALER_ENV_URL_MERCHANT_DONATIONS", "#")
default_ctx["intro_url"] = os.environ.get("TALER_ENV_URL_INTRO", "#")
default_ctx["bank_url"] = os.environ.get("TALER_ENV_URL_BANK", "#")
default_ctx["auditor_url"] = os.environ.get("TALER_ENV_URL_AUDITOR", "#")
default_ctx["currency"] = os.environ.get("TALER_CONFIG_CURRENCY", "??")
auditor_priv_file = os.path.expanduser("~/.local/share/taler/auditor/offline-keys/auditor.priv")
default_ctx["auditor_pub"] = subprocess.check_output(["gnunet-ecc", "-p", auditor_priv_file]).decode("utf-8").strip()


for in_file in glob.glob("*.j2"):
    name, ext = re.match(r"(.*)\.([^.]+)$", in_file.rstrip(".j2")).groups()
    tmpl = env.get_template(in_file)

    def self_localized(other_locale):
        """
        Return URL for the current page in another locale.
        """
        return "../" + other_locale + "/" + in_file.rstrip(".j2")

    def url_localized(filename):
        return "../" + locale + "/" + filename

    def url(x):
        # TODO: look at the app root environment variable
        # TODO: check if file exists
        return "../" + x

    for l in ("en", "de", "it", "es"):
        locale = os.path.basename(l)

        if os.path.isdir(os.path.join("./locale/", locale)):
            tr = gettext.translation("messages",
                                     localedir="locale",
                                     languages=[locale])

            env.install_gettext_translations(tr, newstyle=True)
        else:
            print("warning: locale {} not found".format(locale))
        
        ctx = dict(
                lang=locale,
                url=url,
                self_localized=self_localized,
                url_localized=url_localized,
                filename=name + "." + ext)
        ctx.update(default_ctx)

        content = tmpl.render(**ctx)
        out_name = "./" + locale + "/" + in_file.rstrip(".j2")
        os.makedirs("./" + locale, exist_ok=True)

        with codecs.open(out_name, "w", "utf-8") as f:
            f.write(content)
